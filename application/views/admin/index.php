<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Dashboard</title>

    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>

    <button><a href=<?php echo base_url('/') ?> >Registration page</a></button>

    <h1>Admin Dashboard</h1>

    <table>
        <tr>
            <th colspan="2">Active and Verified user count</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    echo $activeUser; 
                ?>
            </td>
        </tr>
    </table>
    
    <br>

    <table>
        <tr>
            <th colspan="2">User attached with active product</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    echo $userWithProduct; 
                ?>
            </td>
        </tr>
    </table>

    <br>

    <table>
        <tr>
            <th colspan="2">Active product count</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    echo $activeProduct; 
                ?>
            </td>
        </tr>
    </table>

    <br>

    <table>
        <tr>
            <th colspan="2">Active product don't belong user</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    echo $productWithNoUser; 
                ?>
            </td>
        </tr>
    </table>

    <br>

    <table>
        <tr>
            <th colspan="2">sum of all active attached products</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    echo $productSum; 
                ?>
            </td>
        </tr>
    </table>

    <br>
    
    <table>
        <tr>
            <th colspan="2">Sum of Amount of all active attached products</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    echo $productAmountSum; 
                ?>
            </td>
        </tr>
    </table>

    <br>

    <table>
        <tr>
            <th colspan="2">Sum of Amount of products each attached with user</th>
        </tr>
        <?php foreach($userProductAmountSums as $userProductAmountSum) {?>
        
        <tr>
            <td>
                <?php 
                    echo $userProductAmountSum->first_name; 
                ?>
            </td>
            <td>
                <?php 
                    echo $userProductAmountSum->total; 
                ?>
            </td>
        </tr>
        <?php } ?>
    </table>

    <br>

    <table>
        <tr>
            <th colspan="2">Euro to Usa and RON</th>
        </tr>
        <tr>
            <td>
                USD
            </td>
            <td>
                <?php 
                    echo $result->rates->USD; 
                ?>
            </td>
        </tr>
        <tr>
            <td>
                RON 
            </td>
            <td>
                <?php 
                    echo $result->rates->RON; 
                ?>
            </td>
        </tr>
    </table>
</body>
</html>