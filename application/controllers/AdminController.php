<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {

    /* display login page */
	public function index()
	{
		/* active user count */
		$data['activeUser'] = $this->db->get_where('register', array('is_active' => 1, 'is_varified' => 1))->num_rows();

		/* user with active product count */
		$data['userWithProduct'] = $this->db->query("SELECT count(DISTINCT register.id) as count FROM register JOIN user_product ON register.id=user_product.user_id JOIN product ON user_product.product_id=product.id WHERE register.is_active = 1 AND register.is_varified = 1 AND product.is_active = 1")->row()->count;

		/* active product count */
		$data['activeProduct'] = $this->db->get_where('product', array('is_active' => 1))->num_rows();

		/* active product with no attached user */
		$data['productWithNoUser'] = $this->db->query("SELECT COUNT(id) as count FROM `product` WHERE is_active = 1 AND id NOT IN ( SELECT product_id FROM user_product )")->row()->count;

		/* active product sum with attached user */
		$data['productSum'] = $this->db->query("SELECT SUM(user_product.quantity) as total FROM `product` JOIN user_product ON product.id=user_product.product_id JOIN register ON user_product.user_id=register.id WHERE register.is_active = 1 AND register.is_varified = 1  AND product.is_active = 1")->row()->total;

		$data['productAmountSum'] = $this->db->query("SELECT SUM(product.price * user_product.quantity) as total FROM `product` JOIN user_product ON product.id=user_product.product_id JOIN register ON user_product.user_id=register.id WHERE register.is_active = 1 AND register.is_varified = 1  AND product.is_active = 1")->row()->total;

		$data['userProductAmountSums'] = $this->db->query("SELECT register.first_name, SUM(product.price * user_product.quantity) AS total FROM `product` JOIN user_product ON product.id = user_product.product_id JOIN register ON user_product.user_id = register.id WHERE register.is_active = 1 AND register.is_varified = 1  AND product.is_active = 1 GROUP BY register.id")->result();
		
		/* api  */
		/* API URL */
        $url = 'http://api.exchangeratesapi.io/v1/latest?access_key=2fc63ee8947041e6a97442dd82f32184&symbols=USD,RON';
   
        /* Init cURL resource */
        $ch = curl_init($url);
            
        /* set the content type json */
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            
        /* set return type json */
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
        /* execute request */
        $result = curl_exec($ch);

        /* close cURL resource */
        curl_close($ch);

		$data['result'] = json_decode($result);

		$this->load->view('/admin/index', $data);
	}

}
