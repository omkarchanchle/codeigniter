<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends CI_Controller {

    /* product add to cart */
	public function addToCart()
	{
		$data = array(
            'quantity' => $this->input->post('quantity'),
            'product_id' => $this->input->post('product_id'),
            'user_id' => $this->session->userdata('user_id'),
        );
        
		$insert = $this->db->insert('user_product', $data);

        if($insert) {
            echo $this->input->post('quantity')." quantity added of product ".$this->input->post('product_id');
        } else {
            echo "Please try again later"; 
        }
	}
}
