<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

    /* display login page */
	public function index()
	{
		$data['products'] = $this->db->get('product')->result();
		$this->load->view('/user/index', $data);
	}

}
