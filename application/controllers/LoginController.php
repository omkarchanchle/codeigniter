<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

    /* display login page */
	public function index()
	{
		$this->load->view('login');
	}

	/* login auth */
	public function login()
	{
        $email = $this->input->post('email');
        $password = $this->input->post('password');
		
        $user = $this->db->get_where('register', array('email' => $email, 'password' => md5($password), 'is_active' => 1	, 'is_varified' => 1))->first_row();

		/* if valid user */	
        if(!empty($user)) {

			$this->session->set_userdata('user_id', $user->id);

			/* check if admin or user */
			if($user->is_admin == 1) {
				redirect('admin/index');
			} else {
				redirect('user/index');
			}

        } else {

			/* flash message */
            $this->session->set_flashdata('msg', 'Invalid username or password.'); 

            /* redirect to registration page */
            redirect('login');
        }
	}
}
