<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistrationController extends CI_Controller {
	
    /* display registration page */
	public function index()
	{
		$this->load->view('registration');
	}

    /* store registration data and send email */
	public function store()
	{
		$data = array(
            'first_name' => $this->input->post('firstname'),
            'last_name' => $this->input->post('lastname'),
			'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
        );
        
		$insert = $this->db->insert('register', $data);

        /* if store success then send email */
		if($insert) {

            /* send emails */
            $emailRes = $this->sendEmail($data['email'], $this->db->insert_id());

            /* email */
            $this->session->set_flashdata('msg', $emailRes); 

            /* redirect to registration page */
			redirect('/');
		} else {

            /* flash message */
            $this->session->set_flashdata('msg', 'Please try again later.'); 

            /* redirect to registration page */
			redirect('/');
		}
	}

    /* send mail */
	public function sendEmail($email, $id)
	{

        // PHPMailer object
        $mail = $this->phpmailer_lib->load();
        
        // SMTP configuration
        $x = $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'omkar.mtzinfotech@gmail.com';
        $mail->Password = 'omkarmtzinfotechelance';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        
        $mail->setFrom('omkar.mtzinfotech@gmail.com', 'Admin');
        
        // Add a recipient
        $mail->addAddress('omkar.elancesolution@gmail.com');
        
        // Email subject
        $mail->Subject = 'Confirm Email';

        // Set email format to HTML
        $mail->isHTML(true);
        
        // Email body content
        $mailContent = base_url('confirm_email/'.$id);
        $mail->Body = $mailContent;
        
        // Send email
        if(!$mail->send()){
            return 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            return 'Confirmation email has been sent.';
        }
	}

    /* confirm email */
    public function confirmEmail()
	{
        /* user id */
        $id = $this->uri->segment(2);

        $this->db->where('id', $id);
        $mailConfirm = $this->db->update('register', array('is_varified' => 1, 'is_active' => 1));

        if($mailConfirm) {

            /* flash message */
            $this->session->set_flashdata('msg', 'Email confirmed.'); 

            /* redirect to registration page */
			redirect('login');

        } else {

            /* flash message */
            $this->session->set_flashdata('msg', 'Please try again later.'); 

            /* redirect to registration page */
			redirect('login');
        }
	}
}